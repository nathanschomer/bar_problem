from customer import Customer
from rewards import Reward
from copy import copy


class Bar:

    def __init__(self, customers=20, optimal_capacity=5,
                 days_per_week=7, reward_type=0,
                 exploration=0.0, learning_rate=0.2,
                 calc_nash=False, seed_ests=False):

        assert customers > 0, "Bar(): Customers must be > 0"
        assert optimal_capacity > 0, "Bar(): Optimal capacity must be > 0"
        assert days_per_week > 0, "Bar(): days per week must be > 0"
        assert 0 <= reward_type <= 2, "Bar(): Reward type must be 0, 1, or 2"
        assert 0 <= exploration <= 1, "Bar(): Exploration must be 0.0 to 1.0"
        assert type(seed_ests) is bool, "Bar(): type(seed_ests) must be bool!"
        assert type(calc_nash) is bool, "Bar(): type(calc_nash) must be bool!"

        self.customers = [Customer(days_per_week=days_per_week,
                          learning_rate=learning_rate,
                          exploration=exploration,
                          seed_ests=seed_ests) for _ in range(customers)]
        self.num_days = days_per_week
        self.capacity = optimal_capacity

        # initialize customer reward
        self.customer_reward = Reward(customers=customers,
                                      optimal_capacity=optimal_capacity,
                                      days_per_week=days_per_week,
                                      reward_type=reward_type)

        # intialize system reward
        self.system_reward = copy(self.customer_reward)
        self.system_reward.reward_type = 2

        self.calc_nash = calc_nash

        # variables useful for system analysis
        self.steps = 0
        self.history = []   # list of all past customer choices
        self.sys_rewards = []
        self.customer_rewards = []
        self.alignments = []
        self.sensitivities = []
        self.nash = []

    def set_learning_rate(self, new_lr):
        for c in self.customers:
            c.lr = new_lr

    def set_exploration_rate(self, new_er):
        for c in self.customers:
            c.exploration = new_er

    def choices_to_bins(self, choices):
        return [choices.count(i) for i in range(self.num_days)]

    def get_customer_choices(self):
        """
        each customer chooses a night to visit the bar

        adds the updated state to self.history and returns it
        """
        num = len(self.customers)
        choices = [self.customers[n].choose_night() for n in range(num)]

        self.history.append(choices)    # useful for system analysis

        return choices

    def step(self):

        self.steps += 1

        # get a choice from each customer
        curr_state = self.get_customer_choices()

        # get reward for each customer
        rewards = self.customer_reward.calc(curr_state)
        self.customer_rewards.append(rewards)

        # get system reward as a benchmark regarless of customer reward type
        #   just use the first element because they're all the same
        tmp = self.system_reward.calc_system(curr_state)[0]
        self.sys_rewards.append(tmp)

        # send rewards to all the customers
        for customer, reward in zip(self.customers, rewards):
            customer.receive_reward(reward)

        # calculate alignments
#        self.alignments.append(self.customer_reward.alignments(curr_state))

        # calculate sensitivies
#        if self.steps > 1:
#            last_state = self.history[-2]
#            sensitivity = self.customer_reward.sensitivity(curr_state,
#                                                           last_state)
#            self.sensitivities.append(sensitivity)

        if self.calc_nash is True:
            self.nash.append(self.customer_reward.is_nash(curr_state))

    def __str__(self):

        if len(self.history) <= 0:
            return "Bar: []"

        bins = self.choices_to_bins(self.history[-1])

        ret = "Week {}\t: [".format(self.steps)
        for b in bins:
            ret += " {} |".format(b)
        ret = ret[:-1] + "]"
        return ret
