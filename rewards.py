from math import exp, floor
import numpy as np


class Reward:

    def __init__(self, customers=20,
                 optimal_capacity=5, days_per_week=7, reward_type=0):

        self.customers = customers
        self.capacity = optimal_capacity    # b
        self.num_days = days_per_week       # K

        _reward_funcs = [self.calc_individual,      # 0
                         self.calc_differential,    # 1
                         self.calc_system]          # 2

        self.calc = _reward_funcs[reward_type]

        self.difference_type = 0

    def choices_to_bins(self, choices):
        return [choices.count(i) for i in range(self.num_days)]

    def calc_daily_reward(self, attendence):
        g_z = self.num_days * attendence * exp((-1*attendence)/(self.capacity))
        return g_z

    def calc_customer_reward(self, choices, customer):

        # current choice this customer made
        curr_choice = choices[customer]

        # how many people attended the day we chose
        attendence = self.choices_to_bins(choices)[curr_choice]

        # calculate reward based on this attendence
        reward = self.calc_daily_reward(attendence)

        return reward

    def gen_other_states(self, choices, customer, include_curr=False):

        # the choice this customer made (day they chose)
        curr_choice = choices[customer]

        # the other choices they could've made
        #   (list of other days they could've chose)
        other_choices = list(range(self.num_days))

        if not include_curr:
            other_choices.remove(curr_choice)

        for oc in other_choices:
            new_choices = choices[:]
            new_choices[customer] = oc
            yield new_choices

        return

    # REWARD CALCULATIONS #
    def calc_individual(self, choices=[]):
        bins = self.choices_to_bins(choices)
        daily_rewards = [self.calc_daily_reward(b) for b in bins]
        rewards = [daily_rewards[choice] for choice in choices]
        return rewards

    def calc_differential(self, choices=[]):

        rewards = []

        for customer, choice in enumerate(choices):

            # current system reward
            G_z = self.calc_system(choices)[customer]

            # current system reward without me
            if self.difference_type == 0:
                G_z_neg_i = self.calc_system(choices,
                                             ignore_customer=customer)[0]
            else:
                other_rewards = [self.calc_system(c)[0] for c in
                                 self.gen_other_states(choices, customer,
                                                       include_curr=True)]
                G_z_neg_i = sum(other_rewards) / len(other_rewards)
                # idx = int(floor(len(other_rewards)/2))

                # G_z_neg_i = median reward of the choices we could've received
                # G_z_neg_i = np.argsort(other_rewards)[idx]

            reward = G_z - G_z_neg_i
            rewards.append(reward)

        return rewards

    def calc_system(self, choices=[], ignore_customer=None):

        bins = self.choices_to_bins(choices)

        if ignore_customer is not None:
            day = choices[ignore_customer]
            bins[day] -= 1

        total_reward = 0
        for b in bins:
            total_reward += self.calc_daily_reward(b)

        # everyone gets the same reward
        rewards = [total_reward for _ in range(self.customers)]

        return rewards

    @staticmethod
    def constrain(val, min_val, max_val):
        return max(min(max_val, val), min_val)

    # PERFORMANCE ANALYSIS #
    def is_nash(self, choices):
        """
        Returns true or false if current system state
            meets nash equilibrium requirements
        """
        for customer, choice in enumerate(choices):
            for new_choices in self.gen_other_states(choices, customer):
                curr_reward = self.calc(choices)[customer]
                new_reward = self.calc(new_choices)[customer]

                if new_reward > curr_reward:
                    return 0
        return 1

    def _calc_alignment(self, customer, choices):

        # current individual reward this customer recieved
        g_z = self.calc_customer_reward(choices, customer)
        # current system reward with this customers choice
        G_z = self.calc_system(choices)[customer]

        # calculate factoredness -- equation from Dr. Kagan Tumer
        alignment = 0
        n = 0
        for new_choices in self.gen_other_states(choices, customer):

            g_z_prime = self.calc_customer_reward(new_choices, customer)
            G_z_prime = self.calc_system(new_choices)[customer]

            alignment += self.constrain(
                (g_z - g_z_prime)*(G_z - G_z_prime), -1, 1)

            n += 1

        alignment /= n

        return alignment

    def alignments(self, choices):
        alignments = []
        for customer in range(self.customers):
            alignments.append(self._calc_alignment(customer, choices))
        return alignments

    def _calc_sentivity(self, customer, _choices, _last_choices):

        # we don't want to modify these lists elsewhere
        #   ... there's probably a better way to do this
        choices = _choices.copy()
        last_choices = _last_choices.copy()

        # calculate current individual reward
        g_z = self.calc_customer_reward(last_choices, customer)

        last_choices[customer], choices[customer] = \
            choices[customer], last_choices[customer]

        # ____only changing myself_____
        # g_i(last_state - customer_last_choice + customer_curr_choice)
        effect_of_customer = abs(g_z - self.calc_customer_reward(last_choices,
                                                                 customer))

        # ____changing everyone else____
        # g_i(curr_state - customer_curr_choice + customer_last_choice)
        effect_of_others = abs(g_z - self.calc_customer_reward(choices,
                                                               customer))

        # calculate learnability -- equation from Dr. Kagan Tumer
        if effect_of_others != 0:
            sensitivity = effect_of_customer / effect_of_others
        else:
            sensitivity = 0

        return sensitivity

    def sensitivity(self, choices, last_choices):
        sensitivities = []
        for customer in range(self.customers):
            sensitivities.append(self._calc_sentivity(customer,
                                                      choices, last_choices))
        return sensitivities
