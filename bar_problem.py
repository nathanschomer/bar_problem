from bar import Bar
import matplotlib.pyplot as plt
import argparse
import numpy as np
import pickle


# defaults
DEF_CUSTOMERS = 50
DEF_CAPACITY = 5
DEF_DAYS = 5
DEF_REWARD_TYPE = 1
DEF_EXPLORATION = 0.1
DEF_LEARNING_RATE = 0.1
DEF_SEED_ESTIMATES = False
DEF_WEEKS = 1000
DEF_PLOT = False


def proc_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("--customers", type=int,
                        default=DEF_CUSTOMERS, help="number of customers")
    parser.add_argument("--capacity", type=int,
                        default=DEF_CAPACITY, help="optimal capacity of bar")
    parser.add_argument("--days", type=int,
                        default=DEF_DAYS, help="days per week bar is open")
    parser.add_argument("--reward_type", type=int,
                        default=DEF_REWARD_TYPE,
                        help="0 => individual, 1 => difference, 2 => system")
    parser.add_argument("--exploration", type=float,
                        default=DEF_EXPLORATION, help="rate of exploration")
    parser.add_argument("--learning_rate", type=float,
                        default=DEF_LEARNING_RATE,
                        help="learning rate of each customer")
    parser.add_argument("--seed_estimates", type=bool,
                        default=DEF_SEED_ESTIMATES,
                        help="If True, customer reward estimate tables will "
                             "randomly initialized. Otherwise, tables will "
                             "be initialize to zero")
    parser.add_argument("--weeks", type=int, default=DEF_WEEKS,
                        help="number of weeks to run simulation")
    parser.add_argument("--plot", type=bool, default=DEF_PLOT,
                        help="show plots or not")
    parser.add_argument("--save", type=str,
                        help="name of file to save this instance to")
    parser.add_argument("--nash", action="store_true",
                        help="calculate nash equilibria")
    return parser.parse_args()


def moving_avg(vals, N):
    return np.convolve(vals, np.ones((N,))/N, mode='valid')


if __name__ == "__main__":

    args = proc_args()

    # create bar with specified parameters
    bar = Bar(customers=args.customers, optimal_capacity=args.capacity,
              days_per_week=args.days, reward_type=args.reward_type,
              exploration=args.exploration, learning_rate=args.learning_rate,
              calc_nash=True, seed_ests=args.seed_estimates)

    # Run for specified number of weeks
    for n, x in enumerate(range(args.weeks)):
        bar.step()

    print(args)

    # Save bar to file if requested
    if args.save:
        with open(args.save+".pkl", 'wb') as outfile:
            pickle.dump(bar, outfile, pickle.HIGHEST_PROTOCOL)

    nash_idxs = np.argwhere(bar.nash).flatten()
    if len(nash_idxs) > 0:
        eq_bins = bar.customer_reward.choices_to_bins(
            list(np.array(bar.history)[nash_idxs[0]]))
        print("Nash Equilibrium State: {}".format(eq_bins))

    if args.plot:

        avg_rewards = []
        for r in bar.customer_rewards:
            avg_rewards.append(sum(r) / len(r))

        sys_rewards = moving_avg(bar.sys_rewards, 50)
        avg_rewards = moving_avg(avg_rewards, 50)

        # plot rewards
        plt.plot(avg_rewards)
        plt.plot(sys_rewards)
        plt.legend(['avg customer reward', 'system reward'])
        plt.xlabel('Week #')
        plt.ylabel('Reward')
        plt.title('Customer Vs System Reward')
        plt.show()

        # plot alignments
        aligns = np.array(bar.alignments)
        for n in range(aligns.shape[1]):
            tmp = aligns[:, n]
            tmp = moving_avg(tmp, 50)
            plt.plot(tmp)
        plt.show()

        # plot sensitivies
        arr = np.array(bar.sensitivities)
        arr = np.mean(arr, axis=0)
        arr = np.mean(arr, axis=0)
        print("mean sensitivity = {}".format(arr))



        # heatmap of customer reward estimates
        #estimates = [c.reward_estimates for c in bar.customers]
        #fig = px.imshow(estimates)
        #fig.show()
