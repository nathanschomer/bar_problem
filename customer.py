from random import choice, random, seed
from time import time


class Customer:

    def __init__(self, days_per_week=7,
                 learning_rate=0.2, exploration=0.0, seed_ests=False):

        if seed_ests:
            self.reward_estimates = [random() for _ in range(days_per_week)]
        else:
            self.reward_estimates = [0 for _ in range(days_per_week)]

        self.num_days = days_per_week
        self.choice = None
        self.lr = learning_rate
        self.exploration = exploration
        seed(time())

    def choose_best(self):
        # choose best option from reward_estimates
        #   if there are equal options, pick randomly from them
        max_est = max(self.reward_estimates)
        max_indices = [n for n, est in
                       enumerate(self.reward_estimates) if est == max_est]
        best_choice = choice(max_indices)
        return best_choice

    def choose_night(self):

        if random() < self.exploration:
            self.choice = choice(range(self.num_days))
        else:
            self.choice = self.choose_best()

        return self.choice

    def receive_reward(self, reward):
        self.reward_estimates[self.choice] *= (1-self.lr)
        self.reward_estimates[self.choice] += self.lr*reward
